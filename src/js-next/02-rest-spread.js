/**
 * Return an array containing the function arguments
 */
exports.argsAsArray = function(...args) {
    return args;
};

/**
 * Return an array containing the function arguments,
 * but discarding the first two
 */
exports.lastArgs = function(first, second, ...args) {
    return args;
};

/**
 * Return a function which applies the provided transform function
 * on all arguments and returns an array with the results
 */
exports.transformArgs = function(transform) {
    return (...args) => args.map(transform);
};

/**
 * Invoke the callback function passing each character
 * from the provided string as an argument
 */
exports.spreadChars = function(str, cb) {
    cb(...str);
};

/**
 * Concatenate the provided arrays using the spread operator
 */
exports.mergeArrays = function(arr1, arr2) {
    return [...arr1, ...arr2];
};

/**
 * Return array of chars from the provided array of strings
 * using the spread operator
 */
exports.spreadArrayStrings = function(arr) {
    return arr.reduce((str1, str2) => [...str1, ...str2]);
};

/**
 * Flatten an array of arrays using the spread operator
 */
exports.flattenArray = function(arr) {
    const spread = (arr1, arr2) => [...arr1, ...arr2];
    
    return arr.reduce(spread);
};

