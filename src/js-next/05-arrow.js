/**
 * Fix the behaviour of the function
 */
exports.getPerson = function(years, time) {

    return {
        name: 'Pedro',
        age: 5,
        growUp: function() {
            setTimeout(() => {
                this.age += years;
            }, time);
        }
    };
};

/**
 * Fix the behaviour of the function
 */
exports.getArgsAsArray = function() {
    return (...args) => args;
};

/**
 * Fix the behaviour of the function
 */
exports.getMultiplier = function() {

    return {
        factor: 1,
        getMultiplierFunc: function() {
            return (value) => this.factor * value;
        }
    };
};

/**
 * Fix the behaviour of the prefixArray method
 */
exports.Prefixer = function Prefixer(prefix) {
    this.prefix = prefix;
};

exports.Prefixer.prototype.prefixArray = function(arr) {
    const prefixValue = (value) => this.prefix + value;

    return arr.map(prefixValue);
};
