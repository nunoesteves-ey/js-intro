/**
 * Create a Prefixer class with constructor and a prefixArray method
 */
exports.Prefixer = class Prefixer {

    constructor(prefix) {
        this.prefix = prefix;
    }

    prefixArray(arr) {
        return arr.map((v) => this.prefix + v);
    }
};

/**
 * Create a PrefixerSuffixer class which extends from the Prefixer class
 * but adds a sufixArray method
 */
exports.PrefixerSufixer = class PrefixerSufixer extends exports.Prefixer {

    constructor(prefix, suffix) {
        super(prefix);
        
        this.suffix = suffix;
    }

    sufixArray(arr) {
        return arr.map((v) => v + this.suffix);
    }
};
