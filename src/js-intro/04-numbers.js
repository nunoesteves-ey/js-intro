/**
 * Convert a binary String to a Number
 */
exports.binaryToDecimal = function(str) {
    return parseInt(str, 2);
};

/**
 * Add two Numbers with a precision of 2
 */
exports.add =  function(a, b) {
    return (a*10**2 + b*10**2) / 10**2;
};

/**
 * Multiply two Numbers with a precision of 4
 */
exports.multiply =  function(a, b) {
    return (a*10**4 * b*10**4) / 10**(4*2);
};
